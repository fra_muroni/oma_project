package vrptw;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Solution implements Comparable<Solution>{
	
	private List<Vehicle> vehicles;
	private double totalLength;
	private boolean isFeasible;
	
	public Solution(Customer[] customers) {
		int[] ids = new int[customers.length+1];
		ids[0] = -1;
		for (int i = 1; i < customers.length+1; i++) {
			ids[i] = customers[i-1].getId();
		}
		
		vehicles = new LinkedList<Vehicle>();
		isFeasible = true;
		bestSplit(ids);
		computeTotalLength();
	}
	
	/**
	 * 
	 * @return The total length of the solution
	 */
	public double getTotalLength() {
		return totalLength;
	}
	
	/**
	 * 
	 * @return The array of the vehicles
	 */
	public Vehicle[] getVehicles() {
		Object tmp[] = vehicles.toArray();
		Vehicle vs[] = new Vehicle[tmp.length];
		for (int i = 0; i < tmp.length; i++) {
			vs[i] = (Vehicle) tmp[i];
		}
		return vs;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Iterator<Vehicle> it = vehicles.iterator(); it.hasNext(); ) {
			Vehicle v = it.next();
			sb.append(v.getId());
			sb.append(" : ");
			for (Iterator<Customer> it2 = v.getRoute().iterator(); it2.hasNext(); ) {
				sb.append(it2.next().getId());
				sb.append(' ');
			}
			sb.append('\n');
		}
		return sb.toString();
	}
	
	/**
	 * 
	 * @return true if feasible, false otherwise
	 */
	public boolean isFeasible() {
		return isFeasible;
	}
	
	/**
	 * Test case using a solution to the instance C1_2_1 given by the tabu search algorithm of the master thesis
	 * */
	
	public static void test() {
		
		/*Example solution
		 * each line corresponds to the route of a single vehicle
		 * total length = 4875
		*/
		int temp[] = new int[]{45, 178, 50, 156, 112, 168, 79, 29, 87, 42, 123, 
				170, 134, 152, 40, 153, 169, 89, 15, 59, 149, 
				21, 23, 182, 75, 163, 194, 145, 195, 52, 92, 
				30, 120, 19, 192, 196, 97, 96, 130, 28, 74, 
				62, 131, 44, 102, 146, 68, 76, 
				114, 159, 38, 150, 22, 151, 16, 140, 187, 142, 111, 63, 56, 
				177, 3, 88, 8, 186, 37, 81, 138, 
				161, 104, 18, 54, 185, 127, 98, 157, 137, 183, 
				119, 35, 132, 7, 181, 117, 49, 
				57, 118, 83, 143, 176, 165, 188, 
				101, 144, 180, 84, 191, 125, 4, 72, 108, 
				190, 5, 10, 193, 46, 128, 106, 167, 34, 95, 158, 
				113, 155, 78, 175, 13, 43, 2, 90, 67, 39, 107, 
				73, 116, 12, 129, 11, 6, 70, 
				32, 171, 65, 86, 115, 94, 51, 
				148, 103, 197, 124, 141, 69, 200, 174, 136, 189, 
				20, 27, 80, 31, 25, 172, 77, 110, 162, 
				41, 85, 173, 154, 24, 61, 100, 64, 179, 109, 
				60, 82, 166, 126, 71, 9, 1, 99, 53, 
				133, 48, 26, 14, 105, 198, 
				164, 66, 147, 160, 47, 91, 122, 139, 
				93, 55, 135, 58, 36, 33, 121, 17,
				184, 199};
		
		
		Instance instance = Instance.getInstance();
		try {
			instance.readFile("input/C1_2_1.TXT");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Customer order[] = new Customer[instance.getNCustomer()];
		for (int i = 0; i < temp.length; i++) {
			order[i] = instance.getCustomers()[temp[i]-1];
		}
		Solution s = new Solution(order);
		System.out.println(s);
		System.out.println("Total length : " + s.getTotalLength());
	}
	
	
	/**
	 * Modified version of the splitting algorithm of Prins adapted to Time Windows version of VRP 
	 * @param ids array of the ids of the customers to be split
	 * */
	private void bestSplit(int ids[]) {
		Instance instance = Instance.getInstance();
		Customer customers[] = instance.getCustomers();
		double tmpT;
		int stored;
		Customer c;
		int cur_id;
		int prev_id;
		int v_id = 0;
		int j;
		
		double dist[] = new double[ids.length];
		int prev[] = new int[ids.length];
		dist[0] = 0;
		for (int i = 1; i < dist.length; i++) {
			dist[i] = Double.MAX_VALUE;
		}
		
		for (int i = 1; i < ids.length; i++) {
			tmpT = 0;
			stored = 0;
			
			j = i;
			
			do {
				cur_id = ids[j];
				prev_id = ids[j-1];
				c = customers[cur_id];
				
				if ( i != j)
				if (tmpT - instance.getDistanceFromDepot(prev_id) + instance.getDistance(prev_id, cur_id) > c.getMaxT()) {
					break;
				}
				if (stored + c.getDemande() > instance.getCapacity()) {
					break;
				}
				if (tmpT + instance.getDistanceFromDepot(cur_id) > instance.getDepot().getMaxT()) {
					break;
				}
				
				stored += c.getDemande();
				if (i == j) {
					tmpT = Math.max(instance.getDistanceFromDepot(cur_id), c.getMinT());
					tmpT += c.getServiceT() + instance.getDistanceFromDepot(cur_id);
				}
					
				else {
					tmpT = Math.max(tmpT - instance.getDistanceFromDepot(prev_id) + instance.getDistance(prev_id, cur_id), c.getMinT());
					tmpT += c.getServiceT() + instance.getDistanceFromDepot(cur_id);
				}
				if (dist[i-1] + tmpT < dist[j]) {
					dist[j] = dist[i-1] + tmpT;
					prev[j] = i-1;
				}
				j++;
			} while (j < dist.length);
		}
		
		for (int i = prev.length - 1; i > 0; i--) {
			Vehicle v = new Vehicle(v_id++, instance.getCapacity());
			vehicles.add(v);
			for (j = prev[i]+1; j <= i; j++) {
				if (j == prev[i]+1)
					v.addCustomer(customers[ids[j]], instance.getDistanceFromDepot(ids[j]));
				else 
					v.addCustomer(customers[ids[j]], instance.getDistance(ids[j], ids[j-1]));
			}
			v.setTotalLength(v.getTotalLength() + instance.getDistanceFromDepot(ids[i]));
			i = prev[i]+1;
		}
		
		if (vehicles.size() > instance.getNVehicle()) isFeasible = false;
		
	}
	
	/**
	 * Function that sums the length of the paths of the vehicles
	 * */
	private double computeTotalLength() {
		totalLength = 0;
		for (Iterator<Vehicle> it = vehicles.iterator(); it.hasNext(); ) {
			totalLength += it.next().getTotalLength();
		}
		return totalLength;
	}
	
	


	public int compareTo(Solution o) {
		if(this.totalLength<o.getTotalLength())
			return -1;
		if(this.totalLength>o.getTotalLength())
				return 1;
		return 0;
	}
	
}
