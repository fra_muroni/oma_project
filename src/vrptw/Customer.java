package vrptw;

public class Customer implements Cloneable {

	private int id;
	private int x, y;
	private int demande;
	private int minT, maxT;
	private int serviceT;

	public Customer(int id, int x, int y, int demande, int minT, int maxT,
			int serviceT) {
		super();
		this.id = id;
		this.x = x;
		this.y = y;
		this.demande = demande;
		this.minT = minT;
		this.maxT = maxT;
		this.serviceT = serviceT;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getDemande() {
		return demande;
	}

	public void setDemande(int demande) {
		this.demande = demande;
	}

	public int getMinT() {
		return minT;
	}

	public void setMinT(int minT) {
		this.minT = minT;
	}

	public int getMaxT() {
		return maxT;
	}

	public void setMaxT(int maxT) {
		this.maxT = maxT;
	}

	public int getServiceT() {
		return serviceT;
	}

	public void setServiceT(int serviceT) {
		this.serviceT = serviceT;
	}

	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
	
	public boolean equal(Customer other){
		return this.id==other.getId();
		
	}
}
