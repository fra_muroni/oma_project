package vrptw;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Vehicle {
	private int id;
	private int capacity;
	private List<Customer> customers;
	private double totalLength;
	
	public Vehicle(int id, int capacity) {
		super();
		this.id = id;
		this.capacity = capacity;
		this.customers = new LinkedList<Customer>();
		this.setTotalLength(0);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	/**
	 * Add a customer at the end of the route of the vehicle
	 * @param c the customer to be added
	 * @param d the distance between the new customer and the last one of the route
	 */
	public void addCustomer(Customer c, double d) {
		this.customers.add(c);
		this.setTotalLength(this.getTotalLength() + d);
	}

	public double getTotalLength() {
		return totalLength;
	}
	
	/**
	 * 
	 * @return the list of customers visited by the vehicle
	 */
	public List<Customer> getRoute() {
		return customers;
	}

	public void setTotalLength(double totalLength) {
		this.totalLength = totalLength;
	}
	

	/**
	 * Insert a customer in a given position of the route
	 * @param c the customer to be inserted
	 * @param index the position of the insertion
	 */
	public void insertCustomer(Customer c, int index) {
		Instance instance = Instance.getInstance();
		double new_length = 0;
		Customer prev = instance.getDepot();
		
		customers.add(index, c);
		
		for (Iterator<Customer> it = customers.iterator(); it.hasNext(); ) {
			Customer cur = it.next();
			double dist;
			if (prev.getId() == -1) dist = instance.getDistanceFromDepot(cur.getId());
			else dist = instance.getDistance(prev.getId(), cur.getId());
			new_length += dist;
			prev = cur;
		}
		
		this.setTotalLength(new_length);
	}

}
