package vrptw;

import java.io.*;
import java.util.*;



public class Instance {
	private Customer[] customers;
	//private Vehicle[] vehicles;
	private double[][] distances;
	private double[] distances_from_depot;
	private int nVehicle;
	private int capacity;
	private int nCustomer;
	private Customer depot;
	private static Instance instance = null;
	
	/*private constructor for singleton*/
	private Instance() {}
	
	
	/*static method to get the instance of Instance*/
	public static Instance getInstance() {
		if (instance == null) instance = new Instance();
		return instance;
	}
	
	/*File reading*/ 
	public void readFile(String nameFile) throws IOException{
		
		int id, x, y, demand, minT, maxT, serviceT, i, j;
		double t1, t2 , distance;
		
		List<Customer> temp_customers = new ArrayList<Customer>();
		
		
		/*reading by */
		try {
				Scanner sc = new Scanner(new File(nameFile) );
				
				sc.nextLine();
				sc.nextLine();
				sc.nextLine();
				sc.nextLine();
				
				 nVehicle = sc.nextInt();
				 capacity = sc.nextInt();
				 
				 sc.nextLine();
				 sc.nextLine();
				 sc.nextLine();
				 sc.nextLine();
				 
				 sc.nextInt();
				 x = sc.nextInt();
				 y = sc.nextInt();
				 demand = sc.nextInt();
				 minT = sc.nextInt();
				 maxT = sc.nextInt();
				 serviceT = sc.nextInt();
				 
				 depot = new Customer(-1, x, y, demand, minT, maxT, serviceT);
				 
				 i=0; 	/*counter of customer*/
				 
				 while(sc.hasNext()){
					 id = sc.nextInt()-1;
					 x = sc.nextInt();
					 y = sc.nextInt();
					 demand = sc.nextInt();
					 minT = sc.nextInt();
					 maxT = sc.nextInt();
					 serviceT = sc.nextInt();
					 
					 Customer c = new Customer(id, x, y, demand, minT, maxT, serviceT);
					 temp_customers.add(c);    /*adding the customer in the list then it will be copied in the array*/
					 
					 i++;
				 }
				 
				 nCustomer = i ; /**nCustomer = total number of customers, they have indexes from 0 to nCustomers-1 */ 
				 
				 sc.close();		 
		}catch (IOException e){
			System.err.println("Error: "+ e.getMessage());			
			
		}
		
		/*Creating the array*/
		customers = new Customer[nCustomer];     /*initializing the array*/
		for(i=0;i< nCustomer; i++)
			customers[i]=temp_customers.get(i);
		
		
		/* Creating matrix of distances from depot*/
		
		distances_from_depot = new double[nCustomer];
		
		
		for(i=0; i< nCustomer; i++){
			t1 = Math.abs(customers[i].getX()-depot.getX());
			t2 = Math.abs(customers[i].getY()-depot.getY());
			
			distance = Math.sqrt((t1*t1+t2*t2));
			distances_from_depot[i] = distance;
			
		}
		
		
		/* Creating matrix of distances between customers*/
		
		distances = new double[nCustomer][nCustomer];
		
		
		for(i=0; i< nCustomer; i++){
			for(j=i+1; j< nCustomer; j++){
			t1 = Math.abs(customers[i].getX()-customers[j].getX());
			t2 = Math.abs(customers[i].getY()-customers[j].getY());
			
			distance = Math.sqrt((t1*t1+t2*t2));
			distances[i][j] = distance;
			distances[j][i] = distance;
			}
		}
			/*filling the diagonal with +INF*/
		for(i=0;i < nCustomer; i++){
			distances[i][i]= Double.MAX_VALUE;
		}
		
	}
	
		public Customer[] getCustomers(){
			return customers;
		}
		
		public int getNVehicle(){
			return nVehicle;
		}
		public int getCapacity(){
			return capacity;
		}
		public int getNCustomer(){
			return nCustomer;
		}
		public double getDistance(int i, int j){
			return distances[i][j];   /*should add a control i!=j,  anyway distance i=j = +INF */
		}
		public double getDistanceFromDepot(int i) {
			if (i == -1) return 0;
			return distances_from_depot[i];
		}

		public Customer getDepot() {
			return depot;
		}
		
		
			
}	
		
		
		
		
		


