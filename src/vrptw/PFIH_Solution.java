package vrptw;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class PFIH_Solution {
	
	List<Vehicle> vehicles;
	private double randomness;
	
	public PFIH_Solution() {
		this(0.2);
	}
	
	public PFIH_Solution(double randomness) {
		vehicles = new ArrayList<Vehicle>();
		this.randomness = randomness;
		computeFeasibleSolution();
	}

	public Vehicle[] getVehicles() {
		Object tmp[] = vehicles.toArray();
		Vehicle vs[] = new Vehicle[tmp.length];
		for (int i = 0; i < tmp.length; i++) {
			vs[i] = (Vehicle) tmp[i];
		}
		return vs;
	}
	
	public double getTotalLength() {
		double length = 0;
		Instance instance = Instance.getInstance();
		for (Iterator<Vehicle> it = vehicles.iterator(); it.hasNext();) {
			Vehicle v = it.next();		
			Customer prev = instance.getDepot();
			for (Iterator<Customer> it2 = v.getRoute().iterator(); it2.hasNext();) {
				Customer cur = it2.next();
				length += instance.getDistance(prev.getId(), cur.getId());
				prev = cur;
			}
			length += instance.getDistanceFromDepot(prev.getId());
		}
		return length;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Iterator<Vehicle> it = vehicles.iterator(); it.hasNext(); ) {
			Vehicle v = it.next();
			sb.append(v.getId());
			sb.append(" : ");
			for (Iterator<Customer> it2 = v.getRoute().iterator(); it2.hasNext(); ) {
				sb.append(it2.next().getId());
				sb.append(' ');
			}
			sb.append('\n');
		}
		return sb.toString();
	}
	
	/**
	 * Apply a stochastic version of the Push Forward Insertion Heuristic
	 * to determine a feasible (but not very good) solution
	 */
	private void computeFeasibleSolution() {
		Instance instance = Instance.getInstance();
		Customer customers[] = instance.getCustomers();
		List<Customer> unassigned = new ArrayList<Customer>();
		int v_id = 0;
		
		for (int i = 0; i < customers.length; i++) {
			unassigned.add(customers[i]);
		}
		
		Vehicle v = new Vehicle(v_id++, instance.getCapacity());
		vehicles.add(v);
		int stored = 0;
		double tmpT = 0;
		
		while (!unassigned.isEmpty()) {						
			int rand = (int) (Math.random() * unassigned.size());
			Customer first = unassigned.get(rand);
			
			
			
				if (stored + first.getDemande() > v.getCapacity()) {
					v = new Vehicle(v_id++, instance.getCapacity());
					vehicles.add(v);
					stored = first.getDemande();
					tmpT = 0;
				}
				else stored += first.getDemande();
				
				v.addCustomer(first, instance.getDistanceFromDepot(first.getId()));
				unassigned.remove(first);
				tmpT += Math.max(first.getMinT(), instance.getDistanceFromDepot(first.getId())) + first.getServiceT();
				
				while (!unassigned.isEmpty()) {
					LinkedList<Customer> route = (LinkedList<Customer>) v.getRoute();
					double old_length = v.getTotalLength() + instance.getDistanceFromDepot(route.getLast().getId());	
					
					int index = 0;
					int best_positions[] = new int[unassigned.size()];
					double min_insertions[] = new double[unassigned.size()];
					
					for (int i = 0; i < min_insertions.length; i++) min_insertions[i] = Double.MAX_VALUE;
					
					for (Iterator<Customer> it = unassigned.iterator(); it.hasNext(); ) {
						Customer c = it.next();
						for (int i = 0; i < route.size(); i++) {
							double new_length = tryInsertion(route, i, c);
							new_length += 2 * instance.getDistanceFromDepot(c.getId());
							if (new_length < min_insertions[index] && Math.random() < 1-randomness) {
								min_insertions[index] = new_length;
								best_positions[index] = i;
							}
						}
						index++;
					}
					
					index = 0;
					int max_index = 0;
					double max_diff = -Double.MAX_VALUE;
					Customer max_c = null;
					
					for (Iterator<Customer> it = unassigned.iterator(); it.hasNext(); ) {
						Customer c = it.next();
						double diff = old_length + 2 * instance.getDistanceFromDepot(c.getId()) - min_insertions[index];
						if (diff > max_diff && Math.random() < 2.0) {
							max_diff = diff;
							max_c = c;
							max_index = index;
						}			
						index++;
					}
					
					if (max_c == null || stored + max_c.getDemande() > v.getCapacity()) {
						v = new Vehicle(v_id++, instance.getCapacity());
						vehicles.add(v);
						stored = 0;
						tmpT = 0;
						break;
					}
					else {
						v.insertCustomer(max_c, best_positions[max_index]);
						unassigned.remove(max_c);
						stored += max_c.getDemande();
						
						route.add(best_positions[max_index], max_c);
						
						tmpT = 0;
						Customer prev = instance.getDepot();
						for (Iterator<Customer> it = route.iterator(); it.hasNext(); ) {
							Customer cur = it.next();
							double dist;
							if (prev.getId() == -1) dist = instance.getDistanceFromDepot(cur.getId());
							else dist = instance.getDistance(prev.getId(), cur.getId());
							tmpT = Math.max(cur.getMinT(), tmpT + dist) + cur.getServiceT();
							prev = cur;
						}
						
						route.remove(best_positions[max_index]);
						
					}
				}		
		}
	}
	
	private double tryInsertion(LinkedList<Customer> route, int index, Customer c) {
		Instance instance = Instance.getInstance();
		double new_length = 0;
		double tmpT = 0;
		Customer prev = instance.getDepot();
		
		route.add(index, c);
		
		
		for (Iterator<Customer> it = route.iterator(); it.hasNext(); ) {
			Customer cur = it.next();
			double dist;
			if (prev.getId() == -1) dist = instance.getDistanceFromDepot(cur.getId());
			else dist = instance.getDistance(prev.getId(), cur.getId());
			if (tmpT + dist > cur.getMaxT()) {
				route.remove(index);
				return Double.MAX_VALUE;
			}
			tmpT = Math.max(cur.getMinT(), tmpT + dist) + cur.getServiceT();
			new_length += dist;
			prev = cur;
		}
		
		new_length += instance.getDistanceFromDepot(prev.getId());
		
		route.remove(index);
		
		return new_length;
	}
} 
