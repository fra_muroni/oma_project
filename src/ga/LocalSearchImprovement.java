package ga;

import vrptw.*;

public class LocalSearchImprovement {

	/*
	 * After calling this class, is the GA that verifies that the chromosome is
	 * not already present in the next generation.
	 * 
	 * ImproveChromosome method is invoked whenever the GA proposes a new
	 * chromosome and this is splitted after being tested and found eligible for
	 * acceptance.
	 */

	public Chromosome improveChromosome(Chromosome previous_offspring,
			Vehicle[] vehicles) {
		Chromosome new_offspring = (Chromosome) previous_offspring.clone();
		boolean optimized = false;
		// total length of the previous solution
		Solution previous_solution = new Solution(previous_offspring.getGenes());
		double oldLength = previous_solution.getTotalLength();
		double newLength;

		for (int i = 0; i < vehicles.length; i++) {
			// For each vehicle we try to optimize the path
			Customer[] new_costumers = new_offspring.getGenes();
			Solution new_solution;

			for (int j = 0; j < vehicles[i].getRoute().size() - 1; j++) {
				for (int k = j + 1; k < vehicles[i].getRoute().size(); k++) {

					new_costumers = this.swap(new_costumers, i, j);
					// total length of the possible current solution
					new_solution = new Solution(new_costumers);
					newLength = new_solution.getTotalLength();
					if ((new_solution.isFeasible()) && (oldLength > newLength)) {
						new_offspring.setGenes(new_costumers);
						optimized = true;
						oldLength = newLength;
						break; // First Improvement: jump at the optimization of
								// the next vehicle.
								// If we would adopt the Steepest Descent
								// strategy we can comment "break" instruction.
					}
				}
			}
		}

		if (optimized)
			return new_offspring;
		return previous_offspring;
	}

	private Customer[] swap(Customer[] c, int i, int j) {
		Customer tmp;

		tmp = c[i];
		c[i] = c[j];
		c[j] = tmp;

		return c;
	}

}
