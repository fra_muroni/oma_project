package ga;

import java.util.*;

import vrptw.*;

public class GeneticAlgorithm {

	private int PopLen,N_ind;
	private Chromosome [] population;
	
	private double []prob;
	private double []quality;
	//array of probability to take a solution
	
	
	
	public GeneticAlgorithm(int popLen, int n_ind, List<Customer[]> elements) {
		super();
		PopLen = popLen;
		N_ind = n_ind;
		population=new Chromosome[PopLen];
		prob=new double[PopLen];
		quality=new double[PopLen];
		if(elements!=null){
			Iterator<Customer[]> it=elements.iterator();
			for(int i=0;i<popLen&&it.hasNext();i++){
				population[i]=new Chromosome(it.next(),n_ind);
				Solution s=new Solution(population[i].getGenes());
				quality[i]=s.getTotalLength();
			}
			this.prob_evaluator();
		}
	}
	
	private Customer[] RandPopGen(Customer[] initial){
		Customer[] final_ind=new Customer[N_ind]; //final Customer order
		boolean[] used=new boolean[N_ind];	//array used to check if one customer has been already taken
		int i,j;
		
		for(i=0;i<N_ind;i++){
			j=(int)Math.random()*N_ind;
			while(used[j]==true)	//if the element chosen by random function has been already taken  
				j=j%N_ind;				//it jump to the next unused element creating a circle array avoiding 0
			final_ind[i]=initial[j];
			used[j]=true;
		}
		
		return final_ind;
	}
	
	
	public void Initialize(Customer[] curr_ind){
		Solution sol;
		int i;
		boolean new_comb=false;	//needed to force a new combination of genes
		
		sol=new Solution(curr_ind);
		for(i=0;i<N_ind;i++){

			while(!sol.isFeasible() || new_comb){
				curr_ind=this.RandPopGen(curr_ind);
				sol=new Solution(curr_ind);
				new_comb=false;
			}
			new_comb=true;
			population[i].setGenes(curr_ind);
			quality[i]=sol.getTotalLength();
		}
		
		prob_evaluator();		
		return;
	}
	
	private void prob_evaluator(){
		int i;
		double tot_qlt=0.0;
/*
		//evaluation of probability associated to each chromosome
			for(i=0;i<PopLen;i++)
				tot_qlt+=quality[i];
			//better solutions are those which has lower quality values. In order to give them more probability 
			//has been applied that equation
			for(i=0;i<PopLen;i++)
				prob[i]=(1-(quality[i]/tot_qlt))/(PopLen-1);
			*/
		for(i=0;i<PopLen;i++){
			prob[i]=1/quality[i];
			tot_qlt+=prob[i];
		}
		for(i=0;i<PopLen;i++)
			prob[i]=prob[i]/tot_qlt;

		
		
		
			return;
	}
	
	
	private int chooseparent(){
		int i;
		double rand=Math.random(),curr_prob=0.0;
		for(i=0;i<N_ind-1;i++){
			curr_prob+=prob[i];
			if(rand<curr_prob)
				break;
		}
		return i;
	}
	
	
	private boolean exists(Chromosome a,Chromosome[]pop,int len){
		int i;
		for (i=0;i<len;i++){
			if(a.equal(pop[i]))
				return true;
		}
		return false;
	}
	
	private Chromosome crossover(int rp,int lp){
		int i,j,t,lcp,rcp;
		Chromosome child;
		Customer[] lp_genes,rp_genes,child_genes=new Customer[N_ind];
		lp_genes=population[lp].getGenes();
		rp_genes=population[rp].getGenes();
		
		lcp=(int)(Math.random()*N_ind);
		rcp=(int)(Math.random()*N_ind);
		
		if(lcp==rcp)
			rcp=(rcp+1)%N_ind;
			
		for(i=lcp;i!=rcp;i=(i+1)%N_ind)
			child_genes[i]=lp_genes[i];
		
		//j is the index of current gene of right parent
		//i is the index of current gene of child
		j=rcp;
		while(i!=lcp){
			//evaluate if the i gene of the right parent already exists in the child
			//and find one which is not
				
			for(t=lcp;child_genes[t]!=null;t=(t+1)%N_ind){
				if(rp_genes[j].equal(child_genes[t])){
					j=(j+1)%N_ind;
					t=lcp-1;
				}
			}
			
			child_genes[i]=rp_genes[j];
			j=(j+1)%N_ind;
			i=(i+1)%N_ind;
		}
		
		child=new Chromosome(child_genes,N_ind);
		return child;
		
	}
	
	

	public void Recombine(){
		this.Recombine(100);
	}
	
	public void Recombine(int replace_factor){
		int i,lp,rp;
		Chromosome child;
		Solution s;
		Chromosome[]tmp=new Chromosome[PopLen];
		
		int newpop=PopLen*replace_factor/100;
		
		for(i=0;i<newpop;i++){
			
			do{
				do{
					lp=this.chooseparent();
					do{
						rp=this.chooseparent();
					}while(rp==lp);
					child=crossover(rp,lp);
				}while(exists(child,tmp,i));
				s=new Solution(child.getGenes());
				
			}while(!s.isFeasible());
				
			tmp[i]=child;
		}
		
		for(;i<PopLen;i++){
			do{
				rp=this.chooseparent();
			}while(exists(population[rp],tmp,i));
			tmp[i]=population[rp];
		}
		
		population=tmp;
		this.prob_evaluator();
		return;	
	}
	
	public List<Customer[]> getPopulation(){
		ArrayList<Customer[]> pop=new ArrayList<Customer[]>();
		for(int i=0;i<PopLen;i++){
			pop.add(population[i].getGenes());
		}
		return pop;
	}
	
	
}
