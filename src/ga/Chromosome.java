package ga;

import vrptw.Customer;

public class Chromosome implements Cloneable {
	private Customer[] genes;
	private int N_genes;
	
	public Chromosome(Customer[] genes, int n_genes) {
		super();
		this.genes = genes;
		N_genes = n_genes;
	}

	public Customer[] getGenes() {
		return genes;
	}

	public void setGenes(Customer[] genes) {
		this.genes = genes;
	}

	public Object clone() {
		try {
			Chromosome c = (Chromosome) super.clone();
			c.genes = (Customer[]) genes.clone(); // in dubbio
			return c;
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
	
	public boolean equal(Chromosome a){
		int i;
		Customer[] agenes=a.getGenes();
		for (i=0;i<N_genes ;i++){
			if(!genes[i].equal(agenes[i])){
				return false;
			}
		}
		return true;
	}
}
