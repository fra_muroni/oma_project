package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.util.Iterator;

import javax.swing.JPanel;

import vrptw.Customer;
import vrptw.Instance;
import vrptw.Vehicle;

public class SolutionPanel extends JPanel {
	
	private Vehicle[] vehicles;
	private double x_ratio;
	private double y_ratio;
	private int x_size;
	private int y_size;

	public SolutionPanel(Vehicle[] vehicles) {
		this.vehicles = vehicles;
		Instance instance = Instance.getInstance();
		
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		x_size = (int) d.getWidth() - 100;
		y_size = (int) d.getHeight() - 100;
		
		
		setPreferredSize(new Dimension(x_size, y_size));
		
		double max_x = 0;
		double max_y = 0;
		
		Customer customers[] = instance.getCustomers();
		
		for (int i = 0; i < instance.getNCustomer(); i++) {
			Customer c = customers[i];
			if (c.getX() > max_x) max_x = c.getX();
			if (c.getY() > max_y) max_y = c.getY();
		}
		
		x_ratio = x_size / (max_x+60);
		y_ratio = y_size / (max_y+60);
	}
	
	public void setVehicles(Vehicle v[]) {
		vehicles = v;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Instance instance = Instance.getInstance();
		for (int j = 0; j < vehicles.length; j++) {
			boolean flag = true;
			Customer c1 = null;
			Customer c2 = null;
			Color color = new Color((int)(255.0*Math.random()), (int)(255.0*Math.random()), (int)(255.0*Math.random()));
			//Color color = new Color((int)(255.0*j/vehicles.length), (int)(255.0*j/vehicles.length), (int)(255.0*j/vehicles.length));
			for (Iterator<Customer> it = vehicles[j].getRoute().iterator(); it.hasNext();) {
				c2 = it.next();
				if (flag) {
					c1 = instance.getDepot();
					g.setColor(Color.GREEN);
					g.fillOval((int) ((c1.getX()+30)*x_ratio)-2, (int) ((c1.getY()+30)*y_ratio)-2, 8, 8);
					flag = false;
				}
				else {
					g.setColor(Color.RED);
					g.fillOval((int) ((c1.getX()+30)*x_ratio)-2, (int) ((c1.getY()+30)*y_ratio)-2, 5, 5);
					
				}				
				g.setColor(color);
				g.drawLine((int) ((c1.getX()+30)*x_ratio), (int) ((c1.getY()+30)*y_ratio), (int) ((c2.getX()+30)*x_ratio), (int) ((c2.getY()+30)*y_ratio));
				c1 = c2;
			}
			g.setColor(Color.RED);
			g.fillOval((int) ((c1.getX()+30)*x_ratio)-2, (int) ((c1.getY()+30)*y_ratio)-2, 5, 5);
			g.setColor(color);
			c2 = instance.getDepot();
			g.drawLine((int) ((c1.getX()+30)*x_ratio), (int) ((c1.getY()+30)*y_ratio), (int) ((c2.getX()+30)*x_ratio), (int) ((c2.getY()+30)*y_ratio));
		}
	}

}
