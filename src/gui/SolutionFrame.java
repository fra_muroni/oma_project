package gui;

import java.awt.GridLayout;

import javax.swing.JFrame;

import vrptw.Instance;
import vrptw.Vehicle;

public class SolutionFrame extends JFrame {
	
	private SolutionPanel sp;
	
	public SolutionFrame(Vehicle[] vehicles) {
		sp = new SolutionPanel(vehicles);
		
		add(sp);
		setVisible(true);
		pack();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public void setVehicles(Vehicle v[]) {
		sp.setVehicles(v);
	}

}
