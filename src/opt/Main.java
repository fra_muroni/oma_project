package opt;

import ga.GeneticAlgorithm;
import gui.SolutionFrame;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import vrptw.Customer;
import vrptw.Instance;
import vrptw.PFIH_Solution;
import vrptw.Solution;
import vrptw.Vehicle;


public class Main {
	private static final int Pop_len = 130;
	public static void main(String[] args) {
		Instance instance = Instance.getInstance();
		Customer tmp[];
		try {
			instance.readFile("input/C101.txt");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
		PFIH_Solution s1 = null;
		Solution s2 = null;
		ArrayList<Customer[]> population=new ArrayList<Customer[]>();
		ArrayList<Solution> pop1=new ArrayList<Solution>();
		ArrayList<Solution> pop2=new ArrayList<Solution>();
		for (int j = 0; j < Pop_len; j++) {
			do {
				s1 = new PFIH_Solution(0);
				tmp = new Customer[instance.getNCustomer()];
				int i = 0;
				for (Vehicle v : s1.getVehicles()) {
					for (Iterator<Customer> it = v.getRoute().iterator(); it.hasNext();) {
						tmp[i++] = it.next();
					}
				}
				s2 = new Solution(tmp);
			} while (!s2.isFeasible() || s2.getTotalLength() > 1400);
			System.out.println(s2.getVehicles().length +" "+ s2.getTotalLength());
			new SolutionFrame(s2.getVehicles());
		}
		s2=Collections.min(pop1);
		System.out.println("BEST: "+s2.getVehicles().length +" "+ s2.getTotalLength());
		
		GeneticAlgorithm GA=new GeneticAlgorithm(Pop_len,instance.getNCustomer(),population);
		for(int i=0;i<700;i++)
			GA.Recombine();
		population=(ArrayList<Customer[]>) GA.getPopulation();
		Iterator<Customer[]> it=population.iterator();
		int j=0;
		while(it.hasNext()){
			s2 = new Solution(it.next());
			pop2.add(s2);
			System.out.println(j+1+") "+s2.getVehicles().length +" "+ s2.getTotalLength());
			j++;
		}
		s2=Collections.min(pop2);
		System.out.println("BEST: "+s2.getVehicles().length +" "+ s2.getTotalLength());
		
		
		
	}
}
